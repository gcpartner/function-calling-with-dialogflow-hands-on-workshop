import functions_framework
import os
import smtplib
from email.message import EmailMessage
from email.utils import formataddr
from flask import jsonify


def smtp_connection(smtp_server, smtp_port, username, password):
    session = smtplib.SMTP(smtp_server, smtp_port)
    session.starttls()
    session.login('ubuntu', password)
    return session


def send_email(smtp_session, from_addr, to_addr, subject, body, display_name):
    msg = EmailMessage()
    msg['Subject'] = subject
    msg['From'] = formataddr((display_name, from_addr))
    msg['To'] = to_addr
    html_content = f"""
        <!DOCTYPE html>
        <html>
        <head>
            <title>{subject}</title>
            <style>
            body {{ 
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 20px;
            }}
            </style>
        </head>
        <body>
            {body}
        </body>
        </html>
        """
    msg.set_content(html_content, subtype='html')
    # msg.set_content(body)
    smtp_session.send_message(msg)
    smtp_session.quit()

    print(f"Email sent from {from_addr} to {to_addr}.")


def get_env_vars():
    env_vars: dict = {}
    env_vars['smtp_server']: str = os.environ.get('smtp_server')
    env_vars['smtp_port']: int = int(os.environ.get('smtp_port'))
    env_vars['display_name']: str = os.environ.get('display_name')
    env_vars['from_addr']: str = os.environ.get('from_addr')
    env_vars['password']: str = os.environ.get('password')
    env_vars['subject']: str = os.environ.get('subject')

    return env_vars


def get_post_vars(request):
    request_json: dict = request.get_json()
    post_vars: dict = {}
    post_vars['to_addr'] = request_json['sessionInfo']['parameters']['email']
    post_vars['body'] = request_json['sessionInfo']['parameters']['$request.generative.blood_eligibility_generator_output']
    return post_vars


@functions_framework.http
def main(request):
    req = request.get_json()
    print(f'FULL REQUEST ARGS ARE: {req}')
    env_vars: dict = get_env_vars()
    post_vars: dict = get_post_vars(request)

    smtp_session = smtp_connection(env_vars['smtp_server'], env_vars['smtp_port'],
                                   env_vars['from_addr'], env_vars['password'])
    send_email(smtp_session, env_vars['from_addr'], post_vars['to_addr'],
               env_vars['subject'], post_vars['body'], env_vars['display_name'])
    success_message = f"Your email has been sent to: {post_vars['to_addr']}!"
    return jsonify({"fulfillment_response":{"messages":[{"text":{"text":[success_message]}}]}}), 200
