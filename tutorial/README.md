# Function calling with Dialogflow Hands on Workshop
## Overview
In this training, you'll use Vertex AI Conversation, and Dialogflow CX to build, deploy and configure a virtual agent to assist people who want to donate blood and ensure they meet the required eligibility requirements. The agent will use real public data and Google's generative large language models (LLMs) during Dialogflow CX fulfillment. The model will generate an email and allow you to send it via a webhook you will set up.
![architecture](../img/architecture.jpg)

## Configure the project
For this project we'll be using the Dialogflow API, the Vertex AI Agent Builder API and set up IAM permissions to allow our dialogflow agent to interact with a cloud function and pre-built SMTP server that will allow us to send emails.

You can activate the necessary API's in a GCP project either via API (gcloud or IaC tools like Terraform) or via the Cloud Console UI.

### Note: 
The SMTP server is ephmeral and has been built explicitly for this training. It will be disabled shortly after the training has concluded.

Before you can create a data store agent in Vertex AI Conversation, you need to enable the Dialogflow and Vertex AI Search and Conversation APIs.

### Setup and requirements
For each lab, you get a new Google Cloud project and set of resources for a fixed time at no cost.

Note the lab's access time (for example, 4:00:00), and make sure you can finish within that time.
There is no pause feature. You can restart if needed, but you have to start at the beginning.

1. When ready, click **Start lab**.
2. Note your lab credentials (**Username** and **Password**). You will use them to sign in to the Google Cloud Console.
3. Launch the Google **Cloud Console** in an incognito window by right clicking **Cloud Console** and selecting `Open Link in Incognito Window`

    ![incognito](../img/df_training_cloudconsoleincognito.png)

4. Enable 3rd party Cookies once your incognito window is open

    ![3rd Party Cookies 01](../img/3rd-party-cookies-01.png)  ![3rd Party Cookies 02](../img/3rd-party-cookies-02.png)

5. Authenticate using the credentials provided to you via QwikLabs
   * If you use other credentials, you'll receive errors or **incur charges**.
6. Accept the terms and skip the recovery resource page.

**Note:** Do not click **End Lab** unless you have finished the lab or want to restart it. This clears your work and removes the project.

### Enable Dialogflow
You will first enable Dialogflow in your Google Cloud project and configure the necessary permissions for your Google Cloud Qwiklabs user account.

1. Click on the Cloud Shell icon (Cloud Shell icon) in the top-right corner of the Google Cloud console toolbar.

    ![cloud_shellcursor](../img/df_training_cloudshell.png)

2. Click `CONTINUE` when the Cloud Shell Introductino pops up.
3. To set your project ID and region environment variables, run the following commands:

    ```bash
    PROJECT_ID=$(gcloud config get-value project)
    echo "PROJECT_ID=${PROJECT_ID}"
    ```

4. To store the signed-in Google user account in an environment variable, run the following command:

    ```bash
    USER=$(gcloud config get-value account 2> /dev/null)
    echo "USER=${USER}"
    ```
   * Click Authorize if prompted.

5. Run the following command in the terminal to enable the `dialogflow` API.

    ```bash
    gcloud services enable dialogflow.googleapis.com --project ${PROJECT_ID}
    ```

## Create a new chat app and data store in Vertex AI Agent Builder
The purpose of the agent that you'll build is to assist customers who have questions about blood eligibility. You will use the Australian Red Cross Lifeblood FAQ as the source of truth, this is public data that can be found in a public Google Cloud Storage buckets.  

To create a new chat app in Vertex AI Conversation:

1. Navigate to the Vertex AI Search & Conversation console by typing `Agent Builder` into the top search bar, and selecting `Agent Builder` from the results.
    ![search_and_conversation](../img/df_training_agentbuilder.png)
  * You may be prompted to "**Continue and Activate the API**", if so click that button.
2. Ensure you're in the correct project then click +New app near the top of the console.
   
    ![new_app_image](../img/df_training_newapp.png)

3. From the Vertex AI Conversation console, select Chat as the type of app that you want to create.

    ![chat_app_image](../img/df_training_chatappimage.png)

4. Type a Company name of your choosing for example (`Blood Donation`). This parameter is used to define the company that your agent represents and the scope of your agent.
5. Specify an Agent name of `Blood Donation Agent`.
6. Leave all other settings as they are and click "**Continue**"
7. Click "**Create data store**"

    ![create_data_store_image](../img/dialogflow_training_connect_datastore.png)

8. Select **Cloud Storage** as the data source for your data store.
   * Specify the following Google Cloud Storage folder that contains sample data for this training, and note that the gs:// prefix is not required: `cloud-samples-data/dialogflow-cx/arc-lifeblood`
   * **[Optional]:** Explore this data in your cloud shell using the following command: 
     
    ```bash
    gsutil cat gs://cloud-samples-data/dialogflow-cx/arc-lifeblood
    ```
9. Select Unstructured documents as the type of data you are importing. Click **Continue**.
10. Specify a Data store name of `Australian Red Cross Lifeblood Unstructured`.
11. Click *Create* to create the data store.
12. In the list of data stores, select the newly created `Australian Red Cross Lifeblood Unstructured`.

    ![select_datastore](../img/df_training_selectdatastore.png)

13. Click **Create** to create your chat app.
   * This may take a minute or two to create the app.

### Configure your agent to provide grounded responses
To ensure your agent is responding to the user input using the data sources we can set levels of grounding for the output llm.

1. On the left hand side choose `Agent Builder`, and then click on the App we created in the previous step, in this case, `Blood Donation Agent` (This should open Dialogflow CX in a seperate Tab)
2. Click the agent settings button on the top right.
    ![agent-settings](../img/df_training_agent_settings_button.png)
3. Select the "header" **Generative AI**
4. Select the "header" **Data Store** and check the box that states `Enable Grounding`.
   * Grounding may already be enabled 
5. Change the **lowest score allowed** to `High: we have high confidence the response is grounded`
6. Under **Data Store Prompt** 
   * **Agent Name** reads `Dracula`
   * **Agent identity** reads `chatbot that helps users assess their eligibility for blood donation`
   * **Company Name** reads `Dracula Blood Corp`
   * **Company Description** reads `a company that allows users to donate their blood`
   * **Agent Scope** reads `assessing whether they are eligible to donate blood`
   * You'll see the full generated prompt that will be used off to the right:

    ![prompt-exmaple](../img/df_training_prompt_example.png)
7. Click **Save** at the top.

**Note: It can take up to 4 hours for your documents to be available and ready for use by your agent. (Usually 30min)**

### Enable generative fallback for the Default Start Flow's no-match event
You will not be making any changes during this step, but take note of the following settings.

1. Open the Build tab from your Dialogflow CX console by clicking **Build** at the top left, and then click the  **Start Page**. Click the `sys.no-match-default` event handler. Unless the box is already checked, enable the generative fallback feature.
    ![](../img/dialogflow_training_select_startpage.png)
2. Check the agent's data store
On the Start Page click Edit data stores to look into the Data store settings.


3. Edit data stores

    ![](../img/dialogflow_training_select_datastore.png) 
   * The data store you have previously created has been already selected for you by Dialogflow.
4. Scroll down to Agent Responses under the header: Fulfillment.
   * A fulfillment is the agent response to the end user. Dialogflow has pre populated ***Agent says*** with the parameter `$request.knowledge.answers[0]` which at run time contains the top answer to the user's question.

    ![](../img/dialogflow_training_agentresponses.png)

## Configure the agent to answer eligibility FAQs
Our next task is to design the agent to determine the user's eligibility to donate blood. There are strict requirements donors must meet such as age, weight, existing conditions, recent travels, etc. For the scope of this training we will only consider age and weight. A generator will use Google's large language models (LLMs) to dynamically make an informed decision based on the context of the conversation and the knowledge base.

### Configure new routes and parameters
1. From within the **Edit data stores** section
2. In the **Agent responses** section, change the existing **Agent Says** to `$request.knowledge.answers[0] Would you like to take the eligibility quiz to find out if you can donate blood, and start changing lives?`.
    ![df_agent_image](../img/df_training_agentresponsedatastore.png)
   * $request.knowledge.answers[0] is referencing the first response from the LLM knowledge base, if you would like to access more, you can reference them like this in a generator
   * To do this you would create a bullet point list with $answer1, $answer2 and $answer3 and compare them"
   * Then as a parameter in the generator in your flow you will need to assign the value to $answer1, $answer2 and $answer3 with $request.knowledge.answers[0] $request.knowledge.answers[1] $request.knowledge.answers[2] or in an agent response.

3. Click the **Save** button
4. We now need to design the agent to handle "yes" and "no" responses. To begin with, we will create a **confirmation.yes** intent and a **confirmation.no** intent that routes to a new page.
   * More information [here](https://cloud.google.com/dialogflow/cx/docs/concept/agent-design#intent-reuse) about intent reuse.

5. To create the **confirmation.yes** intent, we'll need to create a new route that transitions to a new page **Eligibility Quiz** first create the **Route** by using the `+` in the `Routes modal` in Dialogflow.
   * Once on the Route Creation screen give the route a Description: `Yes to Eligibility Quiz`
   * Then click the dropdown under **Intent** and click `+ new Intent`
   * Set the Display name as: `confirmation.yes`
    ![confirmation.yes](../img/df_training_confirmationyesTop.png)
   * Under `Training phrases` add the following phrases pressing ***Enter*** between each.
     * yes
	 * yeah
	 * yep
	 * okay
	 * yes I do
	 * you bet
	 * absolutely
	 * yes please
    ![dftraining](../img/df_training_confirmationyesTraining.png)
   * Click ***Save*** at the top, which will take you back to the **Route Creation** step
   * Scroll to the bottom and under the `Transition` section choose **Page**
   * Under the **Page** dropdown select `+ new Page`
   * Name the Page `Eligibility Quiz`
   * Click **Save** at the top
6. As mentioned before, we will simplify the quiz and we will only consider the age and the weight of the user to determine whether or not they're eligible to donate we will denote a parameter that will assume the age and weight are the last piece of information the user provides.
   * On the left click on the **Eligibility Quiz** page and add a new form parameter by clicking the ***+*** to the right of **Parameters**.
   * Name the parameter **age-weight**
   * Choose `@sys.any` as the entity type (Scroll to bottom of list).
   * Under **Initial prompt fulfillment > Agent Responses > Agent says** type:`What is your age and weight?`.
      * We want to collect both age and weight at the same time.
   * Click **Save** at the top.
7. To make the blood donation agent sound more logical, Go back to the **Start Page* and click on the `Default Welcome Intent` and change the Agent Say's section:
   * Start by clicking the `Trash Icon` to the right of `Agent says`
   * Click **Add dialogue option**
   * Choose **Text**
   * Type the following greetings and clicking **Add** after each one.
     * `Hi! I'm a blood donation agent, how are you doing?`
     * `Hello! Would you like to donate blood?`
     * `Good day! What questions do you have about blood donation today?`
     * `Greetings! How can I assist you about blood donation?`
8. Click the **Save** button


## Create and configure the eligibility generator
The [generator feature](https://cloud.google.com/dialogflow/cx/docs/concept/generative/generators) is a Dialogflow CX feature that allows developers to use Google's latest generative large language models (LLMs) during Dialogflow CX fulfillment. Generators to generate agent responses at runtime. A generator can handle generic responses that involve general knowledge from a large textual dataset it was trained on or context from the conversation.

We shall create a new generator that will compare the information provided by the user (such as age and weight) with the eligibility requirements to determine whether the user can donate.

1. On the Dialogflow CX console go to the **Manage** tab on the top left, select **Generators** and click **Create new**.

    ![Manage Generators](../img/df_training_managegenerators.png)

2. Provide a descriptive display name, write the text prompt, & set the LLM model configuration:
   * Display name: `Blood Donation Eligibility`
   * Text prompt:

    ```
    Check the users eligibility against the following criteria: the minimum age is 18 and the maximum age is 75. Weight should be above 50 Kg. The user age and weight is $last-user-utterance. Be nice and tell the user if they are eligible to donate (also tell them why not in case)
    ```

   * The text prompt is sent to the generative model during fulfillment at runtime. It should be a clear question or request in order for the model to generate a satisfactory response. You can use special built-in generator prompt placeholders in your text prompt:
   * **$conversation** The conversation between the agent and the user, excluding the very last user utterance.
   * **$last-user-utterance** The last user utterance.
      * The text prompt you have configured expects the user to provide age and weight in one conversational turn (the `$last-user-utterance`).
   * Token Limit: `> 530`
     * You'll need to click the blue slider to make it move.
     * Dragging or typing the number doen't work for some reason.
   * Click **Save** to create the generator.

### Use the generator in fulfillment and configure all the required parameters
Back in the **Build** tab. Click on the **Eligibility Quiz** page, add a new route.

1. This route will occur when of the all parameters have been filled.
   * Under the **Condition** section create the following rule:
   * **Parameter:** `$page.params.status`
   * **Operand:** `=`
   * **Value:** `"FINAL"`
2. Expand the **Generators** section of the **Fulfillment** pane.
   * Click **Add generator**
   * Select the **Blood Donation Eligibility** generator. 
      * After selecting the generator you need to define the output parameter that will contain the result of the generator after execution.
      * `$request.generative.blood_eligibility_generator_output`
3. Use the defined output parameter under the **Fulfillment** header as the **Agent responses > Agent says**.
   * `$request.generative.blood_eligibility_generator_output`
4. Click **Save**.

5. You are now ready to test it all.
## Test the Agent
The Dialogflow CX Console provides an agent testing modal that enables you to test the latest version of your agent including the pages, intents, parameters registered etc. 

To invoke this select `Test Agent` and start typing as if you were a consumer of your agent.

  ![test_the_agent](../img/df_training_testagentmodal.png)

To see the session parameters being set as you progress through each conversational turn click the horizontal view button

![horizontal_view](../img/dfcx_testing_horizontalview.png)
## Generator prompt tuning
Collecting age and weight in one ago doesn't seem to work unless both age and weight are provided. We should instead create a form that collects both values as entity parameters. To make the prompt contextual of all the eligibility requirements (such as the age and the weight) we can use placeholders by adding a `$` before the word. We will later associate these generator prompt placeholders with session parameters in fulfillment and they will be replaced by the session parameter values during execution.

1. On the left hand pane Open the **Eligibility Quiz** page 
   * Click on the **+** to add a new paramater
     * Display Name: `Age`
     * Entity Type: `@sys.number-integer`
     * Under **Initial prompt fulfillment > Agent Responses > Agent says** type:`What is your age?`.
     * Click **Save**
     ![age_fullfillment](../img/df_training_age_fulfillment.png)
   * Click on the **+** to add a new paramater
     * Display Name: `Weight`
     * Entity Type: `@sys.number-integer`
     * Under **Initial prompt fulfillment > Agent Responses > Agent says** type:`What is your weight in Kilograms?`.
     * Click **Save**
   * **[Do it yourself]** Follow the steps above to create two more paramaters, one named `user_name` to collect the user's name, and the other name `email` for the user's email address.
     * The entity type will need to be `@sys.any` to collect text, and `@sys.email` to collect a valid email address.
     ![paramaters](../img/df_training_paramaters.png)
   * To delete the `age_weight` paramater, you can click on the blue `Parameters` text, and then click the trash icon next to the Paramater you would like to delete. 
2. Before we can change the text prompt of the generator since we are going to add two new custom placeholders we first need to remove the generator from the route fulfillment.
   * Go to the **Manage** tab
   * Select **Generators** 
   * Click the **+ Create** to create a new generator
   * Name it **Blood Donation Eligibility v2**
   * Set the **Text prompt** to following text: 

    ```
    Check the users eligibility against the following criteria: the minimum age is 18 and the maximum age is 75. Weight should be above 50 Kg. 

    The user age is $age and weight is $weight kgs. 

    If the user is eligible, write an email from 'Dracula Blood Corp' congratulating them by name "$user_name on being selected to donate blood, in the voice of Dracula. Ask them to donate blood by reporting only between dusk and dawn to 347 Piccadilly Square, Transylvania, Romania. 

    Output only the text of the email wrapped in HTML paragraph tags, do not include a subject header for this email.
    ```
  * Ensure the **Token Limit** is greater than **530**
  * Click **Save**

**Notice** that we haven't just made the text prompt contextual of the age and weight form parameters, we have also changed the last sentence to be able to generate a formal email to the user which contains the official outcome of the eligibility quiz.

### Update the Generator

1. Click on the **Eligibility Quiz** page, select the **FINAL** route, and expand the **Generators** section of the **Fulfillment** header. Then, update the **Generator** to the new **v2** that we just created. After selecting the generator you will need to map the placeholders with the correct paramaters by selectingt them via the dropdown. Moreover, you need to re-set the output parameter to `$request.generative.blood_eligibility_generator_output` and  Click **Save**.

2. Retest the agent again. The eligibility check takes now into account both age and weight and the wording has changed from a conversational tone to a more polite response that is ready to be sent out without any potential human in the loop.

3. As you test you should be able to see the parameter's being applied to your agent if you change the test view to `horizontal view`.

Example generated email:

```
<p> Greetings, Mena, </p>
<p> I, Dracula, Lord of Darkness, extend my crimson-stained invitation to you. Your age of 55 and weight of 55 kgs meet the criteria for our esteemed blood donation program. </p>
<p> As the night's embrace descends, I beckon you to our hallowed sanctuary at 347 Piccadilly Square, Transylvania, Romania. Only between dusk and dawn shall you present yourself, for it is then that our thirst for sustenance is at its peak. </p>
<p> Your sacrifice will not only replenish our reserves but also grant you eternal gratitude from the denizens of the night. Come, Mena, and let your blood flow freely for the glory of Dracula Blood Corp. </p>
<p> With fangs bared, </p>
<p> Dracula </p>.
```


## Taking action outside Dialogflow through Cloud Functions
### Setting up a Cloud Function
[Google Webhook Documentation](https://cloud.google.com/dialogflow/cx/docs/quick/webhook)

How to use a webhook, so your agent can be more dynamic. Cloud Functions are used to host the webhook due to their simplicity, but there are many other ways that you could host a webhook service. The example also uses the Python programming language, but you can use any language supported by Cloud Functions. 

The example webhook code does the following:

* Reads parameter values from the webhook request.
* Writes a parameter value to the webhook response.
* Provides a text response in the webhook response.

#### Deploy Cloud Function
Now we are going to be deploying the **Google Cloud Function** that receives the webhook and sends the email.

1. **Right click** the link below and select "***Open Link in Incognito Window***"
   * [Open in Cloud Shell](https://shell.cloud.google.com/cloudshell/editor?cloudshell_git_repo=https%3A%2F%2Fgitlab.com%2Fgcpartner%2Ffunction-calling-with-dialogflow-hands-on-workshop&show=ide&cloudshell_workspace=.&cloudshell_open_in_editor=cloud_function_files%2Fenv.yaml&cloudshell_open_in_editor=cloud_function_files%2Fmain.py)
2. Click `I understand` on the Google Cloud welcome message
    ![Welcome to Google Cloud 01](../img/welcome-to-google-cloud-01.png)
3. Check the checkbox to `I agree...`, and click `START CLOUD SHELL`
    ![Open in Cloud Shell 01](../img/open-in-cloud-shell-01.png)
4. Check the checkbox to `Trust repo`, and click `CONFIRM`
    ![Open in Cloud Shell 02](../img/open-in-cloud-shell-02.png)
5. **main.py** has been opened for you to have a look at the function, and what it's doing. The dialogflow webhook fulfillment sends all the parameters that we've set including age, weight, email, and response body to the server, where we can parse these values to take action on them for example sending an email.
   * Nothing needs to be edited in **main.py**
6. Open the **env.yaml** and update the password field from `__REDACTED__` to the password povided by your instructor during the live training.
7. When ready deploy the function using our `gcloud` command open a terminal in the cloud shell editor.
   * Do this by clicking the three lines for menu, clicking `Terminal`, & clicking `New Terminal`

    ![open_terminal](../img/terminal.png)
    
    ```bash
    cd cloud_function_files/
    ```
   * **UPDATE** the following command with your QwikLabs Project ID 
    
    ```bash
    PROJECT_ID=__COPY_FROM_QWIKLABS__
    ```

    ```bash
    gcloud services enable run.googleapis.com --project ${PROJECT_ID}
    gcloud functions deploy df_email_send \
      --gen2 \
      --runtime=python311 \
      --trigger-http \
      --entry-point=main \
      --source=./ \
      --env-vars-file env.yaml \
      --region=us-central1 \
      --project=$PROJECT_ID
    ```
   * Choose **No** when prompted: `Allow unauthenticated invocations of new function [df_email_send]? (y/N)?`
8. To open the **Cloud Function** UI, n the Google Cloud Console search for `Cloud Function` and select **Cloud Function**.
    ![Cloud Functions 00](../img/df_training_cloud_function00.png)
9. You should see your new Cloud Function named **df_email_send**. 
   * Click on the name of the Cloud Function
   * Click **Trigger**
   * Copy the **HTTPS URL**
     * This is what will be used in the Dialogflow CX agent.


### Apply the Cloud Function to a Dialogflow Fulfillment
1. Go back to your chat app by searching for **Agent Builder** like before and clicking on your **Blood Donation Agent** app.
2. Open up the Eligibility Quiz route `$page.params.status = "FINAL"` and under the **Fulfillment** header select **Webhook settings**
3. Toggle the **Enable webhook** button and click **+Create webhook**. 
4. On the webhook creation page, leave all the parameters as they are, paste the **Webhook URL**, and add a display name of your choosing for example `dialogflow-send-email-function`
5. Click the **Save** button at the top.
6. In the tag setting be sure to add the tag value `email`.
7. Click **Save** at the top.
   * The tag is how we parse the correct utterance in the webhook sent to the Cloud Function. 

### Testing your Chat Bot. 
1. Go to the testing menu, go to the testing menu and go through the flow one more time. Try asking alternate questions such as `Can I donate blood if i'm pregnant?`. 
2. You should see a fulfillment message that comes through the bot that says `Your email has been sent to: (YOUR EMAIL)`. This has been sent by the cloud function back to your virtual agent. 

Before finishing, let's check the cloud function. If you go to it in your console, you can click Logs to see all requests as well as the output for any **print()** statements:

![Cloud Functions 01](../img/df_training_cloud_function01.png)

Here is an example of the log history:

![Cloud Functions 02](../img/df_training_cloud_function02.png)

We have added in a **print** statement that outputs the full request body being sent by dialogflow to the cloud function. 

**Note** that you can filter logs per function, per severity, and search via a label or some free-form text.

Congratulations! you have managed to:

* Set up a virtual agent.
* Ground the LLM responses with a Datastore
* Created an interactive series of events learning about page flows, fulfillments and dynamic generators
* Interacted with a backend server to take actions on the data you collected through your virtual agent. 

This is scratching the surface of what is possible in Dialogflow. 

We hope you enjoyed this training. 

Please take a second to fill out [this one question survey](https://docs.google.com/forms/d/1VRL-j9cvPL5OkNeqQ_OKDFXaXWyBv9kIFzQD0RHZCdw/viewform?edit_requested=true)
