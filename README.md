# Function calling with Dialogflow Hands on Workshop
This training is desinged to be run on explore.qwiklabs.com

## Training Overview
* Create a basic chat agent in Dialogflow
* Create a data store agent from unstructured data
   * [Project Configuration](#configure-the-project)
   * [Create the Application](#create-a-new-chat-app-and-data-store-in-vertex-ai-search-and-conversation)
* Use knowledge handlers to allow end-users to have conversations with a virtual agent about the content added to a data store.
   * [Ensure Grounded Application Responses](#configure-your-agent-to-provide-grounded-responses)
   * [Enable Generative Fallback Responses](#enable-generative-fallback-for-the-default-start-flows-no-match-event)
* Configure a generator text prompt and make it contextual by using built-in generator prompt placeholders.
   * [Configure the agent to answer eligibility FAQs](#configure-the-agent-to-answer-eligibility-faqs)
* Mark words as generator prompt placeholders and later associate them with session parameters in fulfillment to use their values during execution.
* Configure a generator to handle responses that involve knowledge from a large textual dataset and context from the current conversation.
   * [Configure new routes and parameters](#configure-new-routes-and-parameters)
   * [Create and configure the eligibility generator](#create-and-configure-the-eligibility-generator)
   * [Use the generator in a fulfillment and configure all the required parameters](#use-the-generator-in-fulfillment-and-configure-all-the-required-parameters)
* Generate a formal email using generators
   * [Generator Prompt Tuning](#generator-prompt-tuning)
* Test your agent and simulate customer questions that trigger generated responses
   * [Create and invoke a cloud function that will allow you to interact with external systems from your dialogflow chatTaking action outside Dialogflow through Cloud Functions](#taking-action-outside-dialogflow-through-cloud-functions)
